// This file is part of Ducks.
// Copyright (C) 2015 Ducks authors.
//
// Ducks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// Ducks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Ducks. If not, see <http://www.gnu.org/licenses/>.

#include "terrain.h"

#include <cmath>

#include <MCAssetManager>
#include <MCGLScene>
#include <MCLogger>

#include <QOpenGLTexture>

namespace {
float backgroundDistance = 100;
}

Terrain::Terrain(QSize size)
    : TerrainGeneratorBase(size)
{
     mMud = QImage(":/images/mud.jpg");
     mMud = mMud.scaled(size);

     mSky = QImage(":/images/sky.jpg");
     mSky = mSky.scaled(size);
     mSky = mSky.mirrored(false, true);
}

void Terrain::generate(MCGLScene & glScene)
{
    MCLogger().info() << "Generating terrain..";

    mAtoms.clear();

    static const float PI = 3.14159265f;
    for (int x = 0; x < size().width(); x++) {
        const float m = std::sin(float(x) * PI * 2.0f / size().width()) *
                std::cos(float(x) * PI * 3.0f / size().width());
        const float a = size().height() * 0.25f;
        const float b = size().height() * 0.5f;
        float h = b + m * a;
        h = h < 0 ? 0 : h;
        h = h > size().height() ? size().height() : h;
        const int groundTopThickness = 10;
        for (int y = h - 1; y >= 0; y--) {
            Atom atom(y < h - groundTopThickness ? Atom::Ground : Atom::GroundTop);
            atom.setLocation(QVector3D(x, y, 0));
            mAtoms.push_back(atom);
        }
    }

    QImage terrainImage = renderToImage();

    // TODO: Might be needed to divide the surface into a couple of tiles due to
    //       huge texture size. Let's use a smaller texture for now.
    MCSurfaceMetaData surfaceMetaData;
    surfaceMetaData.height    = std::pair<int, bool>(size().height(), true);
    surfaceMetaData.width     = std::pair<int, bool>(size().width(), true);
    surfaceMetaData.minFilter = std::pair<GLint, bool>(GL_LINEAR, false);
    surfaceMetaData.magFilter = std::pair<GLint, bool>(GL_LINEAR, false);

    setTerrainSurface(
        MCAssetManager::surfaceManager().createSurfaceFromImage(surfaceMetaData, terrainImage));

    if (!backgroundSurface()) {
        const float parallaxScale = (glScene.eyeZ() + backgroundDistance) / glScene.eyeZ();
        surfaceMetaData.height = std::pair<int, bool>(static_cast<int>(size().height() * parallaxScale), true);
        surfaceMetaData.width  = std::pair<int, bool>(static_cast<int>(size().width() * parallaxScale), true);
        surfaceMetaData.handle = "backgound";
        setBackgroundSurface(
            MCAssetManager::surfaceManager().createSurfaceFromImage(surfaceMetaData, mSky));
    }
}

QImage Terrain::renderToImage()
{
    MCLogger().info() << "Rendering terrain to image..";

    QImage terrain(size(), QImage::Format_ARGB32);
    terrain.fill(0x00000000);

    const int height = size().height();
    const int width = size().width();
    for (auto atom : mAtoms) {
        const int x = atom.location().x();
        const int y = atom.location().y();
        switch (atom.type()) {
        case Atom::Ground:
        {
            const int mudX = x * mMud.width() / width;
            const int mudY = y * mMud.height() / height;
            terrain.setPixel(x, y, mMud.pixel(mudX, mudY));
            break;
        }
        case Atom::GroundTop:
            terrain.setPixel(x, y, 0xffcc8822);
            break;
        default:
            break;
        }
    }
    return terrain;
}

void Terrain::render(MCCamera & camera)
{
    backgroundSurface()->render(
        &camera, MCVector3dF(size().width() / 2, size().height() / 2, -backgroundDistance), 0);

    terrainSurface()->render(&camera, MCVector3dF(size().width() / 2, size().height() / 2), 0);
}
