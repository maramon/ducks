// This file is part of Ducks.
// Copyright (C) 2015 Ducks authors.
//
// Ducks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// Ducks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Ducks. If not, see <http://www.gnu.org/licenses/>.

#include "terraingeneratorbase.h"

#include <cassert>

TerrainGeneratorBase::TerrainGeneratorBase(QSize size)
    : mSize(size)
    , mTerrainSurface(nullptr)
    , mBackgroundSurface(nullptr)
{
}

QSize TerrainGeneratorBase::size() const
{
    return mSize;
}

void TerrainGeneratorBase::setTerrainSurface(MCSurface & surface)
{
    mTerrainSurface = &surface;
}

MCSurface * TerrainGeneratorBase::terrainSurface()
{
    return mTerrainSurface;
}

void TerrainGeneratorBase::setBackgroundSurface(MCSurface & surface)
{
    mBackgroundSurface = &surface;
}

MCSurface * TerrainGeneratorBase::backgroundSurface()
{
    return mBackgroundSurface;
}

TerrainGeneratorBase::~TerrainGeneratorBase()
{
}

