// This file is part of Ducks.
// Copyright (C) 2015 Ducks authors.
//
// Ducks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// Ducks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Ducks. If not, see <http://www.gnu.org/licenses/>.

#ifndef RENDERER_DATA_H
#define RENDERER_DATA_H

#include <QObject>
#include <QSize>

#include "scene.h"

/** Rendering data object exposed to the QML world. */
class RendererData : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QSize size READ size WRITE setSize NOTIFY sizeChanged)

    Q_PROPERTY(QString fps READ fps WRITE setFps NOTIFY fpsChanged)

public:

    RendererData(QObject* parent = nullptr);

    void setSize(QSize size);

    QSize size() const;

    void setScene(ScenePtr scene);

    QString fps() const;

    void setFps(QString fps);

    ScenePtr scene();

    Q_INVOKABLE void initDrag(int touchX, int touchY);

    Q_INVOKABLE void finishDrag(int touchX, int touchY);

    Q_INVOKABLE void dragPosition(int touchX, int touchY);

signals:

    void dragInited(int touchX, int touchY);

    void dragFinished(int touchX, int touchY);

    void dragPositionChanged(int touchX, int touchY);

    void fpsChanged();

    void sizeChanged();

    void sizeChangeRequested(QSize size);

private:

    QSize mSize;

    ScenePtr mScene;

    QString mFps;
};

#endif // RENDERER_DATA_H
