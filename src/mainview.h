// This file is part of Ducks.
// Copyright (C) 2015 Ducks authors.
//
// Ducks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// Ducks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Ducks. If not, see <http://www.gnu.org/licenses/>.

#ifndef MAIN_VIEW_H
#define MAIN_VIEW_H

#include <QQuickView>

class MainView : public QQuickView
{
    Q_OBJECT

protected:

    void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;

    void keyPressEvent(QKeyEvent *event) Q_DECL_OVERRIDE;

    void keyReleaseEvent(QKeyEvent *event) Q_DECL_OVERRIDE;

    bool event(QEvent *e) Q_DECL_OVERRIDE
    {
        int type = e->type();
        if (type == QEvent::Close) {
            e->accept();
            emit quitRequested();
            return true;
        }

        return QWindow::event(e);
    }

signals:

    void keyPressed(int key);

    void keyReleased(int key);

    void resizeRequested(QSize size);

    void quitRequested();
};

#endif // MAIN_VIEW_H
