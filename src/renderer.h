// This file is part of Ducks.
// Copyright (C) 2015 Ducks authors.
//
// Ducks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// Ducks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Ducks. If not, see <http://www.gnu.org/licenses/>.

#ifndef RENDERER_H
#define RENDERER_H

#include <QOpenGLFramebufferObject>
#include <QQuickFramebufferObject>
#include <QMutex>

#include "rendereritem.h"
#include "scene.h"

class RendererData;

class Renderer : public QQuickFramebufferObject::Renderer, protected QOpenGLFunctions
{
public:

    Renderer(RendererData* RendererData);

    virtual ~Renderer();

    QOpenGLFramebufferObject* createFramebufferObject(const QSize &size);

protected:

    /** \reimp */
    void render();

private:

    void init();

    void renderWorld();

    RendererData* mRendererData;

    QOpenGLFramebufferObject* mRootFbo;

    QMutex mLock;
};

#endif // RENDERER_H
