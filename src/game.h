// This file is part of Ducks.
// Copyright (C) 2015 Ducks authors.
//
// Ducks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// Ducks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Ducks. If not, see <http://www.gnu.org/licenses/>.

#ifndef GAME_H
#define GAME_H

#include <QGuiApplication>
#include <QObject>
#include <QTimer>

#include <memory>

#include "mainview.h"
#include "scene.h"
#include "terrain.h"

#include <MCAssetManager>

class RendererData;

class Game : public QObject
{
    Q_OBJECT

public:

    Game(int & argc, char ** argv);

    virtual ~Game();

    int run();

private slots:

    void quit();

private:

    void initLevel();

    void parseArgs(int argc, char ** argv);

    QGuiApplication mApp;

    MCAssetManager mAssetManager;

    RendererData* mRendererData;

    MainView* mMainView;

    bool mFullScreen;
};

#endif // GAME_H
