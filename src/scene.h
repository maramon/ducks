// This file is part of Ducks.
// Copyright (C) 2015 Ducks authors.
//
// Ducks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// Ducks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Ducks. If not, see <http://www.gnu.org/licenses/>.

#ifndef SCENE_H
#define SCENE_H

#include <memory>
#include <string>
#include <unordered_map>

#include "terrain.h"
#include "water.h"

#include <QObject>
#include <QTimer>
#include <QVector2D>

#include <MCCamera>
#include <MCWorld>
#include <MCGLScene>
#include <MCGLShaderProgram>

class RendererData;
class QOpenGLFramebufferObject;

class Scene : public QObject
{
    Q_OBJECT

public:

    explicit Scene(RendererData & rendererData);

    virtual ~Scene();

    void init();

    void update();

    void render(QOpenGLFramebufferObject * rootFbo);

    //! \return shader program object by the given id string.
    MCGLShaderProgramPtr program(const std::string & id);

    const QSize & sceneSize() const;

    const QSize & terrainSize() const;

    const QSize & viewSize() const;

public slots:

    void initDrag(int touchX, int touchY);

    void finishDrag(int touchX, int touchY);

    void changeDragPosition(int touchX, int touchY);

private:

    typedef std::unordered_map<std::string, MCGLShaderProgramPtr> ShaderHash;

    void createProgramFromSource(std::string handle, std::string vshSource, std::string fshSource);

    //! Load custom vertex and fragment shaders.
    void loadShaders();

    void renderTerrain();

    void renderWater();

    RendererData & mRendererData;

    //! Size of the view port in pixels
    QSize mViewSize;

    //! Size of the visible scene in scene units
    QSize mSceneSize;

    //! Size of the terrain area in scene units
    QSize mTerrainSize;

    TerrainGeneratorBase * mTerrain;

    Water mWater;

    MCCamera mCamera;

    MCWorld mWorld;

    MCGLScene mGLScene;

    QVector2D mCameraInitPos;

    QVector2D mCameraVelocity;

    QVector2D mDragInitPos;

    int mDragCounter;

    QTimer mFpsTimer;

    int mFps;

    ShaderHash mShaderHash;
};

typedef std::shared_ptr<Scene> ScenePtr;

#endif // SCENE_H
