// This file is part of Ducks.
// Copyright (C) 2015 Ducks authors.
//
// Ducks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// Ducks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Ducks. If not, see <http://www.gnu.org/licenses/>.

#include "renderer.h"
#include "rendererdata.h"
#include "scene.h"

#include <MCLogger>

Renderer::Renderer(RendererData* rendererData)
    : mRendererData(rendererData)
    , mRootFbo(nullptr)
{
    initializeOpenGLFunctions();
    MCLogger().info() << "OpenGL Version: " << glGetString(GL_VERSION);
}

void Renderer::init()
{
    assert(mRendererData);

    mRendererData->setScene(ScenePtr(new Scene(*mRendererData)));
    mRendererData->scene()->init();
}

void Renderer::renderWorld()
{
    mRendererData->scene()->update();

    mRendererData->scene()->render(mRootFbo);
}

void Renderer::render()
{
    mLock.lock();

    static bool inited = false;
    if (!inited) {
        init();
        inited = true;
    }

    if (mRendererData->scene()) {

        glClearColor(0, 0, 0, 1);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        renderWorld();
    }

    update();

    mLock.unlock();
}

QOpenGLFramebufferObject* Renderer::createFramebufferObject(const QSize &size)
{
    MCLogger().info() << "Creating root FBO..";

    QOpenGLFramebufferObjectFormat format;
    format.setAttachment(QOpenGLFramebufferObject::Depth);
    format.setSamples(1);
    mRootFbo = new QOpenGLFramebufferObject(size, format);
    return mRootFbo;
}

Renderer::~Renderer()
{
}
