// This file is part of Ducks.
// Copyright (C) 2015 Ducks authors.
//
// Ducks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// Ducks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Ducks. If not, see <http://www.gnu.org/licenses/>.

#include "water.h"
#include "scene.h"

#include <MCCamera>

Water::Water()
    : mPhase(0)
{
}

void Water::render(MCCamera & camera, Scene & scene)
{
    const QSize & sceneSize = scene.sceneSize();
    const QSize & terrainSize = scene.terrainSize();
    const QSize & viewSize = scene.viewSize();

    MCGLShaderProgramPtr waterProgram = scene.program("water");
    static MCGLMaterialPtr dummyMaterial(new MCGLMaterial);
    MCSurface waterSurface(dummyMaterial, sceneSize.width(), sceneSize.height());

    glClearColor(0, 0, 0, 0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    const float waterCameraY = terrainSize.height() - camera.y() - camera.height() / 2;
    const float waterCameraX = camera.x() - camera.width() / 2;
    const float viewHToSceneH = static_cast<float>(viewSize.height()) / sceneSize.height();
    const float viewWToSceneW = static_cast<float>(viewSize.width()) / sceneSize.width();
    waterProgram->setCamera(MCVector2dF(waterCameraX * viewWToSceneW, waterCameraY * viewHToSceneH));

    mPhase += 2.0f;
    mPhase = mPhase >= sceneSize.width() ? 0.0f : mPhase;

    const float cycles = 1.0f;
    const float waterAngle = cycles * 2.0f * 3.1415f / sceneSize.width();
    const float amplitude = 15.0f;
    const float waterLevel = 480 * viewHToSceneH;
    waterProgram->setUserData1(MCVector2dF(waterAngle, amplitude));
    waterProgram->setUserData2(MCVector2dF(mPhase, waterLevel));
    waterSurface.setShaderProgram(waterProgram);
    waterSurface.bindMaterial();
    waterSurface.render(nullptr, MCVector3dF(sceneSize.width() * 0.5f, sceneSize.height() * 0.5f, 0), 0);
}

Water::~Water()
{
}
