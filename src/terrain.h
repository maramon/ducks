// This file is part of Ducks.
// Copyright (C) 2015 Ducks authors.
//
// Ducks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// Ducks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Ducks. If not, see <http://www.gnu.org/licenses/>.

#ifndef TERRAIN_H
#define TERRAIN_H

#include "atom.h"
#include "terraingeneratorbase.h"

#include <memory>

#include <QImage>
#include <QSize>

#include <MCSurface>

class MCGLScene;

class Terrain : public TerrainGeneratorBase
{
public:

    explicit Terrain(QSize size);

    virtual void generate(MCGLScene & glScene) override;

    virtual void render(MCCamera & camera) override;

private:

    Q_DISABLE_COPY(Terrain)

    QImage renderToImage();

    typedef std::vector<Atom> AtomMatrix;
    AtomMatrix mAtoms;

    QImage mMud;

    QImage mSky;
};

typedef std::shared_ptr<Terrain> TerrainPtr;

#endif // TERRAIN_H
