// This file is part of Ducks.
// Copyright (C) 2015 Ducks authors.
//
// Ducks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// Ducks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Ducks. If not, see <http://www.gnu.org/licenses/>.

#include "scene.h"
#include "shaders.h"
#include "rendererdata.h"

#include <MCException>
#include <MCLogger>
#include <MCWorldRenderer>

#include <QOpenGLFramebufferObject>

Scene::Scene(RendererData & rendererData)
    : mRendererData(rendererData)
    , mViewSize(rendererData.size())
    , mSceneSize(QSize(1048, 768))
    , mTerrainSize(QSize(2048, 1024)) // This is in scene units, not in pixels
    , mTerrain(new Terrain(mTerrainSize))
    , mDragCounter(0)
    , mFps(0)
{
    mFpsTimer.setInterval(1000);
    connect(&mFpsTimer, &QTimer::timeout, [=](){
        mRendererData.setFps(QString::number(mFps));
        mFps = 0;
    });
}

void Scene::init()
{
    mGLScene.initialize();
    mGLScene.resize(mViewSize.width(), mViewSize.height(), mSceneSize.width(), mSceneSize.height(), 135);

    const int cameraInitX = mTerrainSize.width() / 2;
    const int cameraInitY = mTerrainSize.height() / 2;
    mCamera.init(mSceneSize.width(), mSceneSize.height(), cameraInitX, cameraInitY, mTerrainSize.width(), mTerrainSize.height());

    loadShaders();

    mWorld.renderer().removeParticleVisibilityCameras();
    mWorld.renderer().addParticleVisibilityCamera(mCamera);

    mTerrain->generate(mGLScene);

    connect(&mRendererData, SIGNAL(dragInited(int, int)), this, SLOT(initDrag(int,int)));
    connect(&mRendererData, SIGNAL(dragFinished(int, int)), this, SLOT(finishDrag(int,int)));
    connect(&mRendererData, SIGNAL(dragPositionChanged(int, int)), this, SLOT(changeDragPosition(int,int)));

    mFpsTimer.start();
}

MCGLShaderProgramPtr Scene::program(const std::string & id)
{
    MCGLShaderProgramPtr program(mShaderHash[id]);
    if (!program)
    {
        throw MCException("Cannot find shader program '" + id + "'");
    }
    return program;
}

const QSize & Scene::sceneSize() const
{
    return mSceneSize;
}

const QSize & Scene::terrainSize() const
{
    return mTerrainSize;
}

const QSize & Scene::viewSize() const
{
    return mViewSize;
}

void Scene::createProgramFromSource(std::string handle, std::string vshSource, std::string fshSource)
{
    // Inject precision qualifiers
#ifdef __MC_GLES__
    QString origVsh(vshSource.c_str());
    origVsh.replace("#version 120", "#version 100\nprecision mediump float;\nprecision mediump int;\n");
    vshSource = origVsh.toStdString();

    QString origFsh(fshSource.c_str());
    origFsh.replace("#version 120", "#version 100\nprecision mediump float;\nprecision mediump int;\n");
    fshSource = origFsh.toStdString();
#endif

    // Note: ShaderProgram throws on error.
    MCGLShaderProgram * program = new MCGLShaderProgram(vshSource, fshSource);
    mShaderHash[handle].reset(program);
}

void Scene::loadShaders()
{
    // Custom shaders
    createProgramFromSource("fbo", Shaders::fboVsh, Shaders::fboFsh);
    createProgramFromSource("water", Shaders::fboVsh, Shaders::waterFboFsh);
}

void Scene::render(QOpenGLFramebufferObject * rootFbo)
{
    static QOpenGLFramebufferObject mainFbo(mViewSize);

    mainFbo.bind();
    renderTerrain();
    mainFbo.release();

    static QOpenGLFramebufferObject waterFbo(mViewSize);
    static MCGLMaterialPtr dummyMaterial(new MCGLMaterial);
    dummyMaterial->setTexture(mainFbo.texture(), 0);

    waterFbo.bind();
    renderWater();
    waterFbo.release();

    // We have to bind the FBO of QQuickFramebufferObject again here, because
    // it got trashed when we bound our fbo.
    rootFbo->bind();
    glClearColor(0, 0, 0, 0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    dummyMaterial->setTexture(mainFbo.texture(), 0);
    dummyMaterial->setTexture(waterFbo.texture(), 1);
    MCSurface ss(dummyMaterial, mSceneSize.width(), mSceneSize.height());
    ss.setShaderProgram(program("fbo"));
    ss.bindMaterial();
    ss.render(nullptr, MCVector3dF(mSceneSize.width() / 2, mSceneSize.height() / 2, 0), 0);
}

void Scene::renderTerrain()
{
    mTerrain->render(mCamera);
}

void Scene::renderWater()
{
    mWater.render(mCamera, *this);
}

void Scene::update()
{
    mDragCounter++;
    mCameraVelocity *= 0.9f;

    const QVector2D newPos = QVector2D(mCamera.x(), mCamera.y()) + mCameraVelocity;
    mCamera.setPos(newPos.x(), newPos.y());

    mFps++;
}

void Scene::initDrag(int touchX, int touchY)
{
    mCameraVelocity = QVector2D();
    mCameraInitPos  = QVector2D(mCamera.x(), mCamera.y());
    mDragInitPos    = QVector2D(touchX, touchY);
    mDragCounter    = 1;
}

void Scene::finishDrag(int touchX, int touchY)
{
    mCameraVelocity = (mDragInitPos - QVector2D(touchX, touchY)) / mDragCounter;
}

void Scene::changeDragPosition(int touchX, int touchY)
{
    const QVector2D newPos = mCameraInitPos - QVector2D(touchX, touchY) + mDragInitPos;
    mCamera.setPos(newPos.x(), newPos.y());
}

Scene::~Scene()
{
    delete mTerrain;
}
