// This file is part of Ducks.
// Copyright (C) 2015 Ducks authors.
//
// Ducks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// Ducks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Ducks. If not, see <http://www.gnu.org/licenses/>.

#include "game.h"
#include "mainview.h"
#include "rendereritem.h"
#include "rendererdata.h"
#include "scene.h"

#include <cstdlib>
#include <string>
#include <vector>

#include <QDesktopWidget>
#include <QScreen>
#include <QQuickView>
#include <QQmlContext>
#include <QTextStream>

#include <MCLogger>

Game::Game(int & argc, char ** argv)
    : mApp(argc, argv)
    , mRendererData(new RendererData)
    , mFullScreen(false)
{
    parseArgs(argc, argv);

    int defaultViewWidth = 1280;
    int defaultViewHeight = 768;

    if (mFullScreen) {
        defaultViewWidth = QGuiApplication::primaryScreen()->geometry().width();
        defaultViewHeight = QGuiApplication::primaryScreen()->geometry().height();
    }

    mRendererData->setSize(QSize(defaultViewWidth, defaultViewHeight));
}

void Game::parseArgs(int argc, char ** argv)
{
    QTextStream out(stdout);
    const std::vector<std::string> args(argv, argv + argc);
    const int l = static_cast<int>(args.size());
    for (int i = 1; i < l; i++) {
        const std::string arg = args.at(i);
        if (arg == "--help") {
            out << "TODO." << endl;
        } else if (arg == "--fs") {
            mFullScreen = true;
        } else {
            QTextStream err(stderr);
            err << "Unknown argument " << arg.c_str() << endl;
        }
    }
}

int Game::run()
{
    mMainView = new MainView;

    qmlRegisterType<RendererData>("Ducks", 1, 0, "RendererData");
    qmlRegisterType<RendererItem>("Ducks", 1, 0, "RendererItem");

    connect(mMainView, &MainView::quitRequested, this, &Game::quit);

    connect(mMainView, &MainView::keyReleased, [this](int key){
        if (key == Qt::Key_Escape) {
            quit();
        }
    });

    mMainView->rootContext()->setContextProperty("rendererData", mRendererData);
    mMainView->setResizeMode(QQuickView::SizeRootObjectToView);
    mMainView->setSource(QUrl("qrc:///main.qml"));

    if (mFullScreen) {
        mMainView->showFullScreen();
    } else {
        mMainView->show();
    }

    return mApp.exec();
}

void Game::quit()
{
    mApp.quit();
}

Game::~Game()
{
    delete mMainView;
    delete mRendererData;
}
