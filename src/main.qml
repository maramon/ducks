// This file is part of Ducks.
// Copyright (C) 2015 Ducks authors.
//
// Ducks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// Ducks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Ducks. If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.2
import Ducks 1.0

RendererItem {

    width: rendererData.size.width
    height: rendererData.size.height

    Component.onCompleted: {
        setData(rendererData)
    }

    MouseArea {
        anchors.fill: parent
        onPressed: {
            rendererData.initDrag(mouseX, mouseY);
        }
        onReleased: {
            rendererData.finishDrag(mouseX, mouseY);
        }
        onPositionChanged: {
            rendererData.dragPosition(mouseX, mouseY);
        }
    }

    Text {
        id: fpsText
        text: "FPS: " + rendererData.fps
        color: "white"
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.margins: parent.height * 0.01
        font.pixelSize: parent.height * 0.025
    }
}
