// This file is part of Ducks.
// Copyright (C) 2015 Ducks authors.
//
// Ducks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// Ducks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Ducks. If not, see <http://www.gnu.org/licenses/>.

#include "rendererdata.h"

#include <cassert>

RendererData::RendererData(QObject* parent)
    : QObject(parent)
    , mScene(nullptr)
{
}

void RendererData::setSize(QSize size)
{
    mSize = size;
    emit sizeChanged();
    emit sizeChangeRequested(mSize);
}

QSize RendererData::size() const
{
    return mSize;
}

QString RendererData::fps() const
{
    return mFps;
}

void RendererData::setFps(QString fps)
{
    mFps = fps;

    emit fpsChanged();
}

void RendererData::setScene(ScenePtr scene)
{
    mScene = scene;
}

ScenePtr RendererData::scene()
{
    return mScene;
}

void RendererData::initDrag(int touchX, int touchY)
{
    emit dragInited(touchX, touchY);
}

void RendererData::finishDrag(int touchX, int touchY)
{
    emit dragFinished(touchX, touchY);
}

void RendererData::dragPosition(int touchX, int touchY)
{
    emit dragPositionChanged(touchX, touchY);
}
