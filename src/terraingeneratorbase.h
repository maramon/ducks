// This file is part of Ducks.
// Copyright (C) 2015 Ducks authors.
//
// Ducks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// Ducks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Ducks. If not, see <http://www.gnu.org/licenses/>.

#ifndef TERRAINGENERATORBASE_H
#define TERRAINGENERATORBASE_H

#include <QSize>

class MCCamera;
class MCSurface;
class MCGLScene;

class TerrainGeneratorBase
{
public:

    explicit TerrainGeneratorBase(QSize size);

    virtual ~TerrainGeneratorBase();

    QSize size() const;

    virtual void generate(MCGLScene & glScene) = 0;

    virtual void render(MCCamera & camera) = 0;

protected:

    void setTerrainSurface(MCSurface & surface);

    MCSurface * terrainSurface();

    void setBackgroundSurface(MCSurface & surface);

    MCSurface * backgroundSurface();

private:

    Q_DISABLE_COPY(TerrainGeneratorBase)

    QSize mSize;

    MCSurface * mTerrainSurface;

    MCSurface * mBackgroundSurface;
};

#endif // TERRAINGENERATORBASE_H
