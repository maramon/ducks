// This file is part of Ducks.
// Copyright (C) 2015 Ducks authors.
//
// Ducks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// Ducks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Ducks. If not, see <http://www.gnu.org/licenses/>.

#include "rendereritem.h"
#include "rendererdata.h"
#include "renderer.h"

#include <cassert>

#include <MCLogger>

RendererItem::RendererItem(QQuickItem* parent)
    : QQuickFramebufferObject(parent)
    , mRendererData(nullptr)
    , mRenderer(nullptr)
{
    setTextureFollowsItemSize(false);
}

QQuickFramebufferObject::Renderer* RendererItem::createRenderer() const
{
    assert(mRendererData);
    return new ::Renderer(mRendererData);
}

void RendererItem::setData(RendererData* rendererData)
{
    mRendererData = rendererData;

    connect(mRendererData, &RendererData::sizeChangeRequested, [=](QSize size){
        this->setSize(size);
    });
}
