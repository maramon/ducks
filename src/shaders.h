// This file is part of Ducks.
// Copyright (C) 2015 Ducks authors.
//
// Ducks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// Ducks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Ducks. If not, see <http://www.gnu.org/licenses/>.

#ifndef SHADERS_H
#define SHADERS_H

namespace Shaders {

static const char * fboVsh =
"#version 120\n"
""
"attribute vec3 inVertex;\n"
"attribute vec2 inTexCoord;\n"
"uniform vec4 scale;\n"
"uniform mat4 vp;\n"
"uniform mat4 model;\n"
"varying vec2 texCoord0;\n"
""
"void main()\n"
"{\n"
"    gl_Position = vp * model * (vec4(inVertex, 1) * scale);\n"
"    texCoord0 = inTexCoord;\n"
"}\n";

static const char * fboFsh =
"#version 120\n"
""
"uniform sampler2D tex0;\n"
"uniform sampler2D tex1;\n"
"varying vec2 texCoord0;\n"
""
"void main(void)\n"
"{\n"
"    vec4 waterColor = texture2D(tex1, texCoord0);\n"
"    vec4 terrainColor = texture2D(tex0, texCoord0);\n"
"    if (waterColor.a > 0.1) {\n"
"        gl_FragColor = mix(terrainColor, waterColor, 0.25);\n"
"    } else {\n"
"        gl_FragColor = terrainColor;\n"
"    }\n"
"}\n";

static const char * waterFboFsh =
"#version 120\n"
""
"uniform sampler2D tex0;\n"
"uniform vec2 camera;\n"
"uniform vec2 userData1;\n"
"uniform vec2 userData2;\n"
"varying vec2 texCoord0;\n"
""
"void main(void)\n"
"{\n"
"    vec2 vertex = gl_FragCoord.xy;"
"    float angle = userData1.x;"
"    float amplitude = userData1.y;"
"    float time = userData2.x;"
"    float level = userData2.y;"
"    if (vertex.y - camera.y > level + sin(angle * (vertex.x + camera.x + time)) * amplitude) {"
"        float intensity = 0.9 + 0.25 * cos(vertex.x * 0.005);\n"
"        gl_FragColor = vec4(0.0, 1.0 * intensity, 0.0, 1.0);\n"
"    } else {\n"
"        gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);\n"
"    }"
"}\n";

}

#endif // SHADERS_H
